<?php

use App\EchoService;
use PHPUnit\Framework\TestCase;

class EchoServiceTest extends TestCase
{
    public function test_for_echo()
    {
        $result = (new EchoService())->say('test!!!');
        $this->assertEquals('test!!!', $result);
    }
}
